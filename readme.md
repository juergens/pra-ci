




## using the presentation

setup

		sudo apt install npm
    sudo npm install -g reveal-md

run

    reveal-md slides.md

compile to html

    reveal-md slides.md --static build
    firefox build/index.html

(for convienince the build will also be checked into git)
