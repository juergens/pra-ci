

# Praktikum - CI und DevOps

Idee: Im SS 2018 wird ein CI-Praktikum am Teco angeboten.

---

##  todo:

# ⬇

----

### BYOD?

Studierende müssen Zugriff einen Linux-Rechner haben, auf dem Docker installiert ist. (Live-Linux mit persistenten Daten ist OK.)

----

### Abgrenzung

* Option 1: Alles auf gitlab.com
  - --> man kommt komplett ohne Docker aus
- Option 2: gitlab im Docker
  - --> man hat volle Kontrolle und lernt auch gleich Docker

---

## Organisatorisches

# ⬇

----

### Lernziele

Die Studierenden verstehen die Konzepte von Continues Integration.

Nach erfolgreichem Abschluss des Praktikums, können die Studenten:

* Issues in einer CI-Pipeline lösen
* mit Docker Containern arbeiten
* bestehende Software in einen CI Workflow einbetten
* CI-Pipelines aufsetzen
* neue Software Projekte in einem CI-System entwickeln

----

### Roadmap

* KW 50: Chefetage redet über dieses Thema
* bis KW 1: Ideen sammeln
* ab KW 2: Organisatorisches klären
* KW 15?: Semesterbeginn


### Modus

Jede 2. Woche (Optional): 45 Minuten Vortrag über Konzept + 45 Minuten fragen zum Übungsblatt

Jede 2. Woche (Pflicht): Jeder Student führt seine Lösung auf einem Live-Linux auf einem Teco-Rechner aus, und zeigt es dem Betreuer, und erklärt dem Betreuer sein Programm

----

#### Pflich-Vorraussetzung:

* Kenntnisse von SWT 1

----

#### Vorteilige Kenntnisse:

* Linux (Distro egal, da 90% in Commandline)
* shell skripte
* Arbeit als Software-Entwickler
* Arbeit an Opensource Projekten
* Installation und Warting von CI-Pipelines

----

#### verwendete Technologien.

Im laufe des Praktikums wird der Student mit folgenden Technologien in Kontakt kommen:

* Docker
* Ubuntu / Fedora / CentOs
* Gitlab
* Git
* Bash
* Python

----

Am Ende der Veranstaltung wird der Student Grundlegenden Kenntnisse mit diesen Technologien haben. Vorkenntnisse zu diesen Techniken sind von Vorteil, jedoch nicht Vorraussetzung für eine erfolgreiche Teilnahme an der Veranstaltung.

----

### Einordnung im Studium:

* Ideal:
  - im 3. Semester (vielleicht zu Ambitioniert, da viele Pflichtmodule zu der Zeit)
  - vor oder während  PSE
* Nach PSE:
  * PSE-Projekte sind perfekte Kandidaten, um sie in CI umzubauen.
* im späteren Verlaufe des Studiums
* Hiwi - Programmierer
* Hiwi - Administrator

---

## Warum?

# ⬇

----

### die Welt vor CI

"Never change a running system"

"fixed old bugs and added some new ones to fix later."

"Dein Vorgänger hat das Projekt betreut und jetzt weiß keiner mehr, wie man das kompiliert. "

"Seit dem letztem Update von libXYZ geht nichts mehr."

"Nur für diesen Fehler lohnt es sich nicht zu production zu deployen. "

----

### Vorteile von CI

CI ist zentraler Bestandteil aller modernen Enwicklungsprozesse.

CI + Docker --> Ops becomes a None-Issue --> "DevOps" --> SysOps can focus on essentials like orchestration and physical machines

----

### CI and you

Schlechtes Dev Ops: "Wir sparen uns das Geld für SysOp, in dem wir unsere Entwickler _DevOps_ nennen"

Gutes Dev Ops: Dev kümmert sich um Code. SysOps kümmert sich um Server. DevOps ist automatisiert.

----

### Wer macht DevOps:

Das aufsetzen der Pipeline ist Chef-Sache. Die einfach Programmierer brauch nur damit arbeiten, muss es aber nicht vestehen.

* In kleinen Firmen (<50 Programmierer): CTO
* in großen Firmen: Technical lead

----

### The Joel Test

 https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/

Etwas veraltet, da von 2000. Heute sind die meisten Punkte soweit verbreitet, dass sie i.d.R. nicht mehr explizit erwähnt werden.

----

 Legende:

 * ✓: Yes
 * (✓): technical basis is ready, but requires some organisational effort
 * ✓✓: double yes

TODO: Schriftgröße verkleinern

----

Question | CI + gitlab
---- | --- | ----
Do you use source control?                           | ✓
Can you make a build in one step?                    | ✓✓ (zero steps)
Do you make daily builds?                            | ✓✓ (build for every commit)
Do you have a bug database?                          | ✓
Do you fix bugs before writing new code?             | (✓)

----

Question | CI + gitlab
---- | --- | ----
Do you have an up-to-date schedule?                  | (✓)
Do you have a spec?                                  | (✓)
Do programmers have quiet working conditions?        |
Do you use the best tools money can buy?             | ✓
Do you have testers?                                 | (✓)

----

Question | CI + gitlab
---- | --- | ----
Do new candidates write code during their interview? |
Do you do hallway usability testing?                 |

----

--> Dieses Praktikum löst 50% der Herausforderungen eines Softwareentwicklers. Die restlichen Herausforderungen sind keine technisches Probleme.

---

## Warum [software]?

# ⬇

----

### gitlab VS github:

* Geheimhaltung: Firmen schicken ungerne Geheimnisse ans Internet; gitlab kann local laufen
* Abschreiben: gitlab erlaubt private repos
* opensource: gitlab ist gnu, (maybe?)

----

### jenkins vs jira vs gitlab-ci-runner:

* gitlab-ci-runner gibt's zu gitlab gratis dazu

----

### Programmiersprache:

* Vermutlich bash
* Projekt basieren vermutlich auf python --> Man muss es zumindest lesen können

---

## Inhalte

# ⬇

----

* Über CI
  * (als Vortrag)
* Arbeiten in einer CI-Pipeline
  * ein Übungsblatt
* Ein Projekt in ein CI-Projekt konvertieren
 * Details: Nächste Folie
 * 2-3 ÜÜbungsblätter
* CI-System aufsetzen
	* 2-3 ÜÜbungsblätter

----

* Ein Projekt in ein CI-Projekt konvertieren:
  - git repository anlegen
  - docker-image erstellen
    - legacy code verstehen
    - ausführen
  - unit test ausführen
  - docker-repository erstellen
  - build&tests automatisieren
  - CI-pipeline einrichten
  - CD-p

---


### Projekt

# ⬇

----

Jeder Student darf sich aussuchen, auf welches Projekt er das CM-Praktikum anwendet. Ab dem 2. Übungsblatt wird es darum gehen dieses Projekt in eine CI-Pipeline einzubauen. Wenn jemand kein eigenes Projekt mitbringt, wird ihm ein Teco-Projekt zugewiesen (vielleicht chronocoomand?). Einzelne Aufgaben kann man auch mit einem anderen Projekt lösen.

----

Die Studenten sollten Ihr Wunsch-Projekt direkt zum ersten Meeting mitbringen und dort vorstellen.

----

Möglichkeiten:

* am Teco? (chronocommand?)
  * Jedes Projekt, was am Teco entwickelt wurde, wäre ein guter Kandidat
* auf github?
* neues dummy-project?
* beliebiges PSE (insbesondere woran der Student bereits arbeitete)

----

Anforderung/Wünschenswertes an Projekten:

* Ausführbar & kompilierbar unter Linux
  * c# ist schwierig kompilierbar in Docker-container
* hat unit tests
  * müssen nicht vollständig sein. Man kann zur Not auch als spontan einen dummy-test anlegen
* ist bereits ausführbar/kompilierbar


----


* ist ein vergangenes PSE-Projekt
* ist kein aktuelles PSE-Projekt
* ist ein Service / ist keine Desktop-Anwendung
  * lässt sich besser mit der docker-registry vereinbaren.
  * Zur Not kann man die Aufgabe mit der Registry auch für ein anderes Projekt bearbeiten

---

## Übungsblätter / Aufgaben

# ➡

---

### Aufgabe "Der Alltag mit CI"

# ⬇

----

Gegeben: gitlab-issue

Gesucht: Lösung in Production

Hintergrund: in dem github ist bereits ein super tolles CI eingerichtet. Der Student braucht "nur" einen merge-request mit dem richtigen commit aufmachen.


----

Aufgaben:

* clone das repo auf gitlab
* clone das repo lokal
* erstelle unit-test, der den Fehler findet,
* commit, push, merge-request im upstream aufmachen
* Ergbnis von CI-Runner anschauen
* korrigiere den Fehler local, commit, push
* CI-Runner soll feststellen, dass Unittests jetzt Erfolgreich sind

---

### Aufgabe "Aufsetzen von gitlab"

# ⬇

----

Aufgaben:

* Installiere Docker
* Erstelle eigenes Dockerfile für gitlab (FROM ubuntu #oder irgendwas, wo es kein gutes Dockerfile auf github gibt)
  * Alternativ: Fertige images benutzen, da Docker out-of-scope in CI-Praktikum
* Führe gitlab in einem Container aus
* Erstelle Konfigurationsdatei für gitlab, und binde sie in den container ein

Anmerkung: Die Daten müssen nicht persistent sein.

----

Optionen:

* Offline-Installation:
	* Beginne mit einem vorgegebenen Base-Image
	* Installiere keine weiteren Packete
	* Alle installierten Programme müssen zusammen mit dem Dockerfile ausgeliefert werden (also in einem Unterordner, wo die binaries/sourcen/zips liegen)

----

Abgabe:

* Zip-Datei:
  - install.sh: installiert Docker
  - Dockerfile: enthält gitlab-server
  - config/: configurationsdateien für gitlab


----

Erfolgskontrolle:

* Das Skript zur installation von Docker wird in einem Live-Linux (ubuntu 18.04) ausgeführt werden
* es wird ein Browser geöffnet mit "firefox http://0.0.0.0:80"
* Nun wird die Startseite einer lebeneden gitlab-instanz angezeigt


		unzip abgabe.zip && cd abgabe
		chmod +x/install.sh && sudo ./install.sh
		sudo docker build -t gitlab . && sudo docker run -it gitlab
		firefox http://0.0.0.0:80

---

### Aufgabe "Die eigene Serverfarm"

# ⬇

----

Aufgaben:


* Installiere Docker-compose
* Starte den Container von Blatt 1 über compose-Datei (Version 3?)
* Teile in 3 Services ein: DB, nginx, gitlab
* Persistente volumes für DB

----

Abgabe

Zipdatei:

* install.sh: Skript, dass Docker und Docker-compose installiert
* docker-compose.yml
* services/:
  - gitlab/
  - nginx/
  - data/

---

## todo (Aufgaben)

Die folgenden Aufgaben müssen noch etwas detailleirter gemacht werden.


# ⬇

----

### Aufgabe "erstellen eines CI-Runners"

* gegeben: Project auf gitlab
* Gesucht: CI-Runner für Projekt

----

### Aufgabe "Erstellen einer Docker registry"


----

### Aufgabe "Erstellen einer CI-Pipeline"

merge-request --> compilierung --> container --> tests --> push container

----

### Aufgabe "Pipeline anpassen"

Flavortest: "Oh nein! Jemand hat deinem Chef erzählt, dass Code-Coverage existiert. Jetzt ist er total begeistert davon und will in allen Projekt 100% CC erreichen."

Aufgabe:

* Erweiterere das CI-Skript um Code-Coverage
* Die pipeline soll erkennen, welche commits Änderungen enthalten, die nicht von einem Test abgedeckt werden.

----

### Aufgabe "deployment"

(vielleicht serverless?)

* compose-datei erstellen
* .env-file einrichten
* image ziehen und starten


----


### Aufgabe "Full circle"

Gegeben: legacy code als zip-Datei

Gesucht:  vollständige CI/CD Pipeline
